**Proyek UTS : Clone Efootball**

Nama    : Luthfi Raynalfi Firdaus

NIM     : 1187050052

Kelas   : Praktikum PBO A

**Deskrispi Aplikasi**

eFootball adalah sebuah permainan video sepak bola yang dikembangkan oleh Konami. Permainan ini tersedia untuk berbagai platform, termasuk Android. eFootball Android adalah versi permainan yang dirancang khusus untuk pengguna Android.

Dalam eFootball Android, pemain dapat memilih dari banyak klub sepak bola terkenal di seluruh dunia dan bermain melawan pemain lain secara online. Permainan ini menawarkan mode permainan yang berbeda, termasuk mode single player, mode multiplayer, dan mode turnamen.

**Teknologi yang Digunakan**

- Bahasa Pemrograman    : Python

**USE CASE PRIORITY**

## List Use Case Prioritas dari Gameplay eFootball Android

| No.  | Nama Use Case                                 | Prioritas |
| ---- | --------------------------------------------- | --------- |
| 1    | Login                                         | Tinggi    |
| 2    | Registrasi user baru                          | Tinggi    |
| 3    | Melihat profil pemain                         | Sedang    |
| 4    | Bermain pertandingan secara offline           | Tinggi    |
| 5    | Pilih mode permainan                          | Tinggi    |