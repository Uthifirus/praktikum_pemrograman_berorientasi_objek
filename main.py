from game.utils import get_last_login_or_create_last_login_user
from user.user import Player
from menu.menu import MainMenu
from utils import *
import pygame,sys
pygame.init()

pygame.display.set_caption("Menu")

if __name__ == '__main__':
    last_user = get_last_login_or_create_last_login_user()
    if last_user is not None:
        last_user = Player(
            username=last_user['username'],
            atk_power=last_user['atk_power'],
            def_power=last_user['def_power'],
            coins=last_user['coins']
        )

    menu = MainMenu(last_user) 
    menu.menu_started()   