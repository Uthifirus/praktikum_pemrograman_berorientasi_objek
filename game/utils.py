from button import Button
from user.utils import create_new_user, get_user_account
from user.user import Player
from utils import last_login_json_path, clear_screen, get_font

from copy import copy, deepcopy

import json
import pygame
import random
import sys
import time
def update_last_login_dict(username):
    return {
        "last_login": username
    }
    
def save_last_login(username):
    last_login = update_last_login_dict(username)
    
    with open(last_login_json_path, 'w') as json_file:
        json.dump(last_login, json_file, indent=3)

def get_last_login():
    with open(last_login_json_path) as last_user:
        data = json.load(last_user)
    # print(f"Data from last login: {data['last_login']}")
    print(f"GET LAST LOGIN FROM FUNCTION GET_LAST_LOGIN: {data['last_login']}")
    return data['last_login']

## GET LAST LOGIN USER. IF DOESN'T EXIST, CREATE NEW USER AND RETURN NEW USER TO GAME
def get_last_login_or_create_last_login_user():
    last_login = get_last_login()
    print(f"GET LAST LOGIN FROM FUNCTION GET_LAST_LOGIN_OR_CREATE_LAST_LOGIN_USER: {last_login}")
    
    if last_login == "":
        return None
    else: 
        return get_user_account(last_login)
    # return get_user_account(last_login)
