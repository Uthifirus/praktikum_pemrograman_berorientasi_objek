import time, sys
import pygame
import random
from abc import ABC, abstractmethod
from button import Button
from copy import copy, deepcopy
from utils import (write_per_character, clear_screen, get_font,
                   SCREEN, BG, PLAY_IMAGE_PATH, OPTION_IMAGE_PATH, QUIT_IMAGE_PATH, SHOP_IMAGE_PATH,
                   LEFT_ARROW_IMAGE_PATH, RIGHT_ARROW_IMAGE_PATH, FONT_PATH)
from user.user import Player
mark_picked_heroes = {}
class GameAbstract(ABC ):
    @abstractmethod
    def start_game(self):
        pass

class Game(GameAbstract):
    def __init__(self, player1: Player):
        self.__player = player1
        self.__is_loopable = True,
        self.add_time_cd = 500
        self.is_refreshed = False
        self.display_surface = pygame.display.get_surface()
        self.font = pygame.font.Font(r"assets\misc\font.ttf",30)
    def get_is_loopable(self):
        return self.__is_loopable
    
    def get_player(self):
        return self.__player
    
    def set_is_loopbale(self, value: bool):
        self.__is_loopable = value
    
class Medium(Game):
    def __init__(self, player1: Player):
        super().__init__(player1)
        self.__win_chance = 50
        self.__goal_event_chance = 12
        self.__refresh_time = None
        self.__random_goal_event_val = None
        self.__random_goal_val = None
        
    def get_win_chance(self):
        return self.__win_chance
    def get_goal_event_chance(self):
        return self.__goal_event_chance
    def get_refresh_time(self):
        return self.__refresh_time
    def get_random_goal_event_val(self):
        return self.__random_goal_event_val
    def get_random_goal_val(self):
        return self.__random_goal_val
    
    def start_game(self):
        print("MEDIUM MODE GAME")
        count_time = 0
        player_point = 0
        opponent_point = 0
        self.display_surface.blit(BG,(0,0))
        self.refresh_time = pygame.time.get_ticks()
        while count_time <= 90:
            
            self.display_surface.blit(BG,(0,0))
            current_time = pygame.time.get_ticks()
            if current_time - self.refresh_time > self.add_time_cd:
                self.random_goal_event_val = random.randint(1,100)
                print(f"RANDOM GOAL EVENT VAL: {self.random_goal_event_val}")
                if self.random_goal_event_val <= self.get_goal_event_chance():
                    self.random_goal_val = random.randint(1,100)
                    print(f"RANDOM GOAL VAL: {self.random_goal_val}")
                    if self.random_goal_val <= self.get_win_chance():
                        player_point +=1
                    else:
                        opponent_point +=1
                self.is_refreshed = True
                count_time +=1
            if self.is_refreshed:
                self.refresh_time = pygame.time.get_ticks()
                self.is_refreshed = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            count_surf = self.font.render(str(count_time), False, "black")
            count_rect = count_surf.get_rect(topleft=(650, 50))
            player_surf = self.font.render(str(self.get_player().get_username()),False, "black")
            player_rect = player_surf.get_rect(topleft=(425,100))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            # player_point_surf = self.font.render(str(self.random_goal_event_val),False, "black")
            player_point_surf = self.font.render(str(player_point),False, "black")
            player_point_rect = player_point_surf.get_rect(topleft=(425+player_rect.width/2,150))
            opponent_point_surf = self.font.render(str(opponent_point),False, "black")
            opponent_point_rect = player_point_surf.get_rect(topleft=(725+opponent_rect.width/2,150))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            self.show_bar(self.display_surface, count_surf, count_rect)
            self.show_bar(self.display_surface, player_surf, player_rect)
            self.show_bar(self.display_surface, opponent_surf, opponent_rect)
            self.show_bar(self.display_surface, player_point_surf, player_point_rect)
            self.show_bar(self.display_surface, opponent_point_surf, opponent_point_rect)
            pygame.display.update()
            
    def show_bar(self, display_surface, surf, rect):
        display_surface.blit(surf, rect)
        pass
class Hard(Game):
    def __init__(self, player1: Player):
        super().__init__(player1)
        self.__win_chance = 15
        self.__goal_event_chance = 12
        self.__refresh_time = None
        self.__random_goal_event_val = None
        self.__random_goal_val = None
        
    def get_win_chance(self):
        return self.__win_chance
    def get_goal_event_chance(self):
        return self.__goal_event_chance
    def get_refresh_time(self):
        return self.__refresh_time
    def get_random_goal_event_val(self):
        return self.__random_goal_event_val
    def get_random_goal_val(self):
        return self.__random_goal_val
    
    def start_game(self):
        
        count_time = 0
        player_point = 0
        opponent_point = 0
        self.display_surface.blit(BG,(0,0))
        self.refresh_time = pygame.time.get_ticks()
        while count_time <= 90:
            
            self.display_surface.blit(BG,(0,0))
            current_time = pygame.time.get_ticks()
            if current_time - self.refresh_time > self.add_time_cd:
                self.random_goal_event_val = random.randint(1,100)
                print(f"RANDOM GOAL EVENT VAL: {self.random_goal_event_val}")
                if self.random_goal_event_val <= self.get_goal_event_chance():
                    self.random_goal_val = random.randint(1,100)
                    print(f"RANDOM GOAL VAL: {self.random_goal_val}")
                    if self.random_goal_val <= self.get_win_chance():
                        player_point +=1
                    else:
                        opponent_point +=1
                self.is_refreshed = True
                count_time +=1
            if self.is_refreshed:
                self.refresh_time = pygame.time.get_ticks()
                self.is_refreshed = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            count_surf = self.font.render(str(count_time), False, "black")
            count_rect = count_surf.get_rect(topleft=(650, 50))
            player_surf = self.font.render(str(self.get_player().get_username()),False, "black")
            player_rect = player_surf.get_rect(topleft=(425,100))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            # player_point_surf = self.font.render(str(self.random_goal_event_val),False, "black")
            player_point_surf = self.font.render(str(player_point),False, "black")
            player_point_rect = player_point_surf.get_rect(topleft=(425+player_rect.width/2,150))
            opponent_point_surf = self.font.render(str(opponent_point),False, "black")
            opponent_point_rect = player_point_surf.get_rect(topleft=(725+opponent_rect.width/2,150))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            self.show_bar(self.display_surface, count_surf, count_rect)
            self.show_bar(self.display_surface, player_surf, player_rect)
            self.show_bar(self.display_surface, opponent_surf, opponent_rect)
            self.show_bar(self.display_surface, player_point_surf, player_point_rect)
            self.show_bar(self.display_surface, opponent_point_surf, opponent_point_rect)
            pygame.display.update()
            
    def show_bar(self, display_surface, surf, rect):
        display_surface.blit(surf, rect)
        pass
class Easy(Game):
    def __init__(self, player1: Player):
        super().__init__(player1)
        
        self.__win_chance = 90
        self.__goal_event_chance = 12
        self.__refresh_time = None
        self.__random_goal_event_val = None
        self.__random_goal_val = None
    
    def get_win_chance(self):
        return self.__win_chance
    def get_goal_event_chance(self):
        return self.__goal_event_chance
    def get_refresh_time(self):
        return self.__refresh_time
    def get_random_goal_event_val(self):
        return self.__random_goal_event_val
    def get_random_goal_val(self):
        return self.__random_goal_val
    
    def start_game(self):
        
        count_time = 0
        player_point = 0
        opponent_point = 0
        self.display_surface.blit(BG,(0,0))
        self.refresh_time = pygame.time.get_ticks()
        while count_time <= 90:
            
            self.display_surface.blit(BG,(0,0))
            current_time = pygame.time.get_ticks()
            if current_time - self.refresh_time > self.add_time_cd:
                self.random_goal_event_val = random.randint(1,100)
                print(f"RANDOM GOAL EVENT VAL: {self.random_goal_event_val}")
                if self.random_goal_event_val <= self.get_goal_event_chance():
                    self.random_goal_val = random.randint(1,100)
                    print(f"RANDOM GOAL VAL: {self.random_goal_val}")
                    if self.random_goal_val <= self.get_win_chance():
                        player_point +=1
                    else:
                        opponent_point +=1
                self.is_refreshed = True
                count_time +=1
            if self.is_refreshed:
                self.refresh_time = pygame.time.get_ticks()
                self.is_refreshed = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            count_surf = self.font.render(str(count_time), False, "black")
            count_rect = count_surf.get_rect(topleft=(650, 50))
            player_surf = self.font.render(str(self.get_player().get_username()),False, "black")
            player_rect = player_surf.get_rect(topleft=(425,100))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            # player_point_surf = self.font.render(str(self.random_goal_event_val),False, "black")
            player_point_surf = self.font.render(str(player_point),False, "black")
            player_point_rect = player_point_surf.get_rect(topleft=(425+player_rect.width/2,150))
            opponent_point_surf = self.font.render(str(opponent_point),False, "black")
            opponent_point_rect = player_point_surf.get_rect(topleft=(725+opponent_rect.width/2,150))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            self.show_bar(self.display_surface, count_surf, count_rect)
            self.show_bar(self.display_surface, player_surf, player_rect)
            self.show_bar(self.display_surface, opponent_surf, opponent_rect)
            self.show_bar(self.display_surface, player_point_surf, player_point_rect)
            self.show_bar(self.display_surface, opponent_point_surf, opponent_point_rect)
            pygame.display.update()
            
    def show_bar(self, display_surface, surf, rect):
        display_surface.blit(surf, rect)
        pass