from abc import ABC, abstractmethod

class Player(ABC):
    def __init__(self, name, level):
        self.__name = name
        self.__level = level
        self.__stats = {}
        
    @abstractmethod
    def play_game(self):
        pass
    
    @abstractmethod
    def view_profile(self):
        pass
    
    @abstractmethod
    def set_formation(self, formation):
        pass
    
    def get_name(self):
        return self.__name
    
    def set_name(self, name):
        self.__name = name
        
    def get_level(self):
        return self.__level
    
    def set_level(self, level):
        self.__level = level
        
    def get_stats(self):
        return self.__stats
    
    def set_stats(self, stats):
        self.__stats = stats

# player_list = dict()

class LocalPlayer(Player):
    def __init__(self, name, level, coins):
        super().__init__(name, level)
        self.__coins = coins
        self.__inventory = []
        
    def play_game(self):
        print("Playing game offline...")
        
    def view_profile(self):
        print(f"Name: {self.get_name()}, Level: {self.get_level()}, Coins: {self.__coins}")
        
    def set_formation(self, formation):
        print(f"Setting formation to {formation}")
        
    def get_coins(self):
        return self.__coins
    
    def set_coins(self, coins):
        self.__coins = coins
        
    def get_inventory(self):
        return self.__inventory
    
    def set_inventory(self, inventory):
        self.__inventory = inventory
        
class OnlinePlayer(Player):
    def __init__(self, name, level, points):
        super().__init__(name, level)
        self.__points = points
        
    def play_game(self):
        print("Playing game online...")
        
    def view_profile(self):
        print(f"Name: {self.get_name()}, Level: {self.get_level()}, Points: {self.__points}")
        
    def set_formation(self, formation):
        print(f"Setting formation to {formation}")
        
    def get_points(self):
        return self.__points
    
    def set_points(self, points):
        self.__points = points

class Defender:
    def __init__(self, position, defence):
        self.__position = position
        self.__defence = defence
        
    def defend(self):
        print(f"{self.__position} defends with {self.__defence} rating")

class Midfielder:
    def __init__(self, position, attack):
        self.__position = position
        self.__attack = attack
        
    def defend(self):
        print(f"{self.__position} supports defence with {self.__attack} rating")
        
    def attack(self):
        print(f"{self.__position} attacks with {self.__attack} rating")

class Forward:
    def __init__(self, position, shoot_power):
        self.__position = position
        self.__shoot_power = shoot_power
        
    def attack(self):
        print(f"{self.__position} shoots with {self.__shoot_power} rating")

class Goalkeeper:
    def __init__(self, position, goalkeeping_skill):
        self.__position = position
        self.__goalkeeping_skill = goalkeeping_skill
        
    def defend(self):
        print(f"{self.__position} defends with {self.__goalkeeping_skill} rating")
        
    def save(self):
        print(f"{self.__position} saves with {self.__goalkeeping_skill} rating")

from typing import List

def main():
    players = []
    formations = ["4-4-2", "4-3-3", "3-5-2"]
    
    while True:
        print("=== eFootball Android ===")
        print("1. Add local player")
        print("2. Add online player")
        print("3. View player profile")
        print("4. Play game")
        print("5. Set formation")
        print("6. Exit")
        
        choice = input("Enter your choice: ")
        
        if choice == "1":
            name = input("Enter player name: ")
            level = input("Enter player level: ")
            coins = input("Enter player coins: ")
            player = LocalPlayer(name, level, coins)
            players.append(player)
            print("Local player added.")
            
        elif choice == "2":
            name = input("Enter player name: ")
            level = input("Enter player level: ")
            points = input("Enter player points: ")
            player = OnlinePlayer(name, level, points)
            players.append(player)
            print("Online player added.")
            
        elif choice == "3":
            name = input("Enter player name: ")
            for player in players:
                if player.get_name() == name:
                    player.view_profile()
                    break
            else:
                print("Player not found.")
            
        elif choice == "4":
            name = input("Enter player name: ")
            for player in players:
                if player.get_name() == name:
                    player.play_game()
                    break
            else:
                print("Player not found.")
                    
        elif choice == "5":
            name = input("Enter player name: ")
            for player in players:
                if player.get_name() == name:
                    formation = input("Enter formation (4-4-2, 4-3-3, 3-5-2): ")
                    if formation not in formations:
                        print("Invalid formation.")
                    else:
                        player.set_formation(formation)
                        print("Formation set.")
                    break
            else:
                print("Player not found.")
                    
        elif choice == "6":
            print("Exiting program...")
            break
                    
        else:
            print("Invalid choice.")
            continue
        
        input("Press Enter to continue...")
main()