# No 1

Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat (Lampirkan link source code terkait)

Jawab: 

Kode program dibawah merupakan kode untuk menambah skor antara untuk player atau untuk musuh.

```python 
self.__win_chance = 50
self.__goal_event_chance = 12

if current_time - self.refresh_time > self.add_time_cd:
    self.random_goal_event_val = random.randint(1,100)
    print(f"RANDOM GOAL EVENT VAL: {self.random_goal_event_val}")
    if self.random_goal_event_val <= self.get_goal_event_chance():
        self.random_goal_val = random.randint(1,100)
        print(f"RANDOM GOAL VAL: {self.random_goal_val}")
        if self.random_goal_val <= self.get_win_chance():
            player_point +=1
            else:
                opponent_point +=1
```

# No 2

Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Jawab:

Kode program dibawah merupakan kode untuk menambah skor pemain cara penambahannya adalah dengan cara melakukan random nilai dari angka 1 sampai 100, kemudian meng-assign nilai tersebut kedalam sebuah variable bernama random goal event. Apabila nilai random tersebut lebih besar dari nilai attribut event chance maka tidak akan terjadi peristiwa kemungkinan terjadinya goal. namun apabila nilai random tersebut lebih kecil dari nilai attribut goal event chance maka akan terjadi peristiwa terjadinya goal. Dan setelah itu akan dirandom kembali sebuah nilai yang mana nilai tersebut akan digunakan untuk memilih apakah terjadinya goal akan berpihak kepada musuh ataupun kepada player. Apabila nilai random goal tersebut lebih kecil dari nilai win chance, maka terjadinya goal akan berpihak kepada player namun apabila nilai tersebut lebih besar dari nilai attribut win chance maka terjadinya akan berpihak kepada pemain lawan.

```python
self.__win_chance = 50
self.__goal_event_chance = 12

if current_time - self.refresh_time > self.add_time_cd:
    self.random_goal_event_val = random.randint(1,100)
	print(f"RANDOM GOAL EVENT VAL: {self.random_goal_event_val}")
	if self.random_goal_event_val <= self.get_goal_event_chance():
        self.random_goal_val = random.randint(1,100)
        print(f"RANDOM GOAL VAL: {self.random_goal_val}")
        if self.random_goal_val <= self.get_win_chance():
            player_point +=1
            else:
                opponent_point +=1
```

# No 3

Mampu menjelaskan konsep dasar OOP

Jawab:

Objek: Objek adalah representasi konkret dari suatu konsep atau entitas dalam program, seperti manusia, mobil, atau akun bank. Objek memiliki atribut (data) dan perilaku (metode) yang dapat dimanipulasi dalam program.

Class: Class adalah blueprint atau cetak biru yang digunakan untuk membuat objek. Class mendefinisikan atribut dan metode yang dimiliki oleh objek yang akan dibuat.

Encapsulation: Encapsulation atau enkapsulasi adalah konsep yang mengikat data dan metode ke dalam suatu objek. Ini memungkinkan objek untuk melindungi data internalnya dari modifikasi atau akses langsung dari objek lain.

Inheritance: Inheritance atau pewarisan adalah konsep yang memungkinkan kelas untuk mewarisi atribut dan metode dari kelas lain. Hal ini memungkinkan pengembangan program yang lebih efisien dan modular, dengan menghindari pengulangan kode yang tidak perlu.

Polymorphism: Polymorphism atau polimorfisme adalah kemampuan objek untuk memiliki banyak bentuk atau perilaku. Polimorfisme memungkinkan kelas-kelas yang berbeda untuk memiliki metode yang sama namun menghasilkan hasil yang berbeda.

# No 4

Mampu mendemonstrasikan penggunaan Encapsulation secara tepat (Lampirkan link source code terkait)

Jawab:

Kodingan dibawah merupakan salah satu implementasi dari penggunaan encapsulation yang mana hal itu ditunjukan oleh baris ke-4 sampai baris ke-19, yang mana daris baris ke-4 sampai baris ke-8 merupakan attribut dari kelas medium yang bersifat private. Attribut yang bersifat private tersebut hanya dapat diakses melalui fungsi dari baris ke-10 hingga baris ke-19.

```python
class Medium(Game):
    def __init__(self, player1: Player):
        super().__init__(player1)
        self.__win_chance = 50
        self.__goal_event_chance = 12
        self.__refresh_time = None
        self.__random_goal_event_val = None
        self.__random_goal_val = None
    
    def get_win_chance(self):
        return self.__win_chance
    def get_goal_event_chance(self):
        return self.__goal_event_chance
    def get_refresh_time(self):
        return self.__refresh_time
    def get_random_goal_event_val(self):
        return self.__random_goal_event_val
    def get_random_goal_val(self):
        return self.__random_goal_val

```

# No 5

Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

Jawab:

Kodingan dibawah merupakan salah satu implementasi dari Abstraction. class GameAbstract merupakan class yang bersifat abstract yang mana class ini mengimplementasikan cass ABC yang membuat class GameAbstract ini menjadi class Abstract. Didalam class game Abstract terdapat fungsi abstract yang bernama start_game, fungsi ini harus diimplementasikan ke dalam class yang menginherit class GameAbstract yang mana di dalam kodingan dibawah ini, class Medium harus mengimplementasikan fungsi start_game.

```python
class GameAbstract(ABC ):
    @abstractmethod
    def start_game(self):
        pass

class Game(GameAbstract):
    def __init__(self, player1: Player):
        self.__player = player1
        self.__is_loopable = True,
        self.add_time_cd = 500
        self.is_refreshed = False
        self.display_surface = pygame.display.get_surface()
        self.font = pygame.font.Font(r"assets\misc\font.ttf",30)
    def get_is_loopable(self):
        return self.__is_loopable
    
    def get_player(self):
        return self.__player
    
    def set_is_loopbale(self, value: bool):
        self.__is_loopable = value
    
class Medium(Game):
    def __init__(self, player1: Player):
        super().__init__(player1)
        self.__win_chance = 50
        self.__goal_event_chance = 12
        self.__refresh_time = None
        self.__random_goal_event_val = None
        self.__random_goal_val = None
        
    def get_win_chance(self):
        return self.__win_chance
    def get_goal_event_chance(self):
        return self.__goal_event_chance
    def get_refresh_time(self):
        return self.__refresh_time
    def get_random_goal_event_val(self):
        return self.__random_goal_event_val
    def get_random_goal_val(self):
        return self.__random_goal_val
    
    def start_game(self):
        print("MEDIUM MODE GAME")
        count_time = 0
        player_point = 0
        opponent_point = 0
        self.display_surface.blit(BG,(0,0))
        self.refresh_time = pygame.time.get_ticks()
        while count_time <= 90:
            
            self.display_surface.blit(BG,(0,0))
            current_time = pygame.time.get_ticks()
            if current_time - self.refresh_time > self.add_time_cd:
                self.random_goal_event_val = random.randint(1,100)
                print(f"RANDOM GOAL EVENT VAL: {self.random_goal_event_val}")
                if self.random_goal_event_val <= self.get_goal_event_chance():
                    self.random_goal_val = random.randint(1,100)
                    print(f"RANDOM GOAL VAL: {self.random_goal_val}")
                    if self.random_goal_val <= self.get_win_chance():
                        player_point +=1
                    else:
                        opponent_point +=1
                self.is_refreshed = True
                count_time +=1
            if self.is_refreshed:
                self.refresh_time = pygame.time.get_ticks()
                self.is_refreshed = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            count_surf = self.font.render(str(count_time), False, "black")
            count_rect = count_surf.get_rect(topleft=(650, 50))
            player_surf = self.font.render(str(self.get_player().get_username()),False, "black")
            player_rect = player_surf.get_rect(topleft=(425,100))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            # player_point_surf = self.font.render(str(self.random_goal_event_val),False, "black")
            player_point_surf = self.font.render(str(player_point),False, "black")
            player_point_rect = player_point_surf.get_rect(topleft=(425+player_rect.width/2,150))
            opponent_point_surf = self.font.render(str(opponent_point),False, "black")
            opponent_point_rect = player_point_surf.get_rect(topleft=(725+opponent_rect.width/2,150))
            opponent_surf = self.font.render("Computer",False, "black")
            opponent_rect = opponent_surf.get_rect(topleft=(725,100))
            self.show_bar(self.display_surface, count_surf, count_rect)
            self.show_bar(self.display_surface, player_surf, player_rect)
            self.show_bar(self.display_surface, opponent_surf, opponent_rect)
            self.show_bar(self.display_surface, player_point_surf, player_point_rect)
            self.show_bar(self.display_surface, opponent_point_surf, opponent_point_rect)
            pygame.display.update()
```

# No 6

Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat (Lampirkan link source code terkait)

Jawab:

Kodingan dibawah merupakan kodingan dari inheritance dan polymorphism yang mana implementasi inheritance ditunjukan pada baris ke-7, baris ke-24, sampai baris ke-27. Kodingan pada baris-baris tersebut menunjukan implementasi inheritance karena class MainMenu menginherit class Menu. Oleh karena itu fungsi - fungsi yang ada di dalam class Menu dan attribut - attribut yang ada di dalam class Menu dapat digunakan oleh class MainMenu tanpa harus di inisiasi kembali didalam class MainMenu.

Sedangkan untuk implementasi polymorphism ditunjukan oleh kode pada baris ke-30 sampai baris ke-74 yang mana fungsi pada baris ke-30 sampai baris ke-74 mengoverride fungsi yang ada didalam class MenuAbstact.

```python
class MenuAbstract(ABC):
    @abstractmethod
    def menu_started(self):
        pass
    

class Menu(MenuAbstract):
    def __init__(self, player: Player|None):
        self.__is_loopable = True
        self.__player = player
        
    def get_player(self):
        return self.__player
    
    def set_player(self, player: Player):
        self.__player = player
    
    def get_is_loopable(self):
        return self.__is_loopable
    
    def set_is_loopable(self, value:bool()):
        self.__is_loopable = value
        
class MainMenu(Menu):
    def __init__(self, player: Player):
        self.is_from_beginning=True
        super().__init__(player)
        
    
    def menu_started(self):
        if self.get_player() is None:
            is_login_btn_clicked = create_user()
            print(f"IS LOGIN BTN CLICKED: {is_login_btn_clicked}")
            if is_login_btn_clicked:
                login()
            last_user = get_last_login()
            print(f"GET LAST USER: {last_user}")
            user_account = get_user_account(last_user)
            print(f"GET USER ACCOUNT: {user_account}")
            player = Player(
                username=user_account['username'], 
                atk_power=user_account['atk_power'], 
                def_power=user_account['def_power'],
                coins=user_account['coins'])
            self.set_player(player)
        print(f"HELLO WORLD BEFORE WHILE SELF.IS_LOOPABLE")
        # time.sleep(3)
        while True:
            SCREEN.blit(BG,(0,0))
            
            MENU_MOUSE_POS = pygame.mouse.get_pos()
            
            # button_list = [USERNAME_BTN, SHOP_BTN,  QUIT_BTN] = create_button([
            button_list = [USERNAME_BTN, SHOP_BTN, LEFT_ARROW_BTN, RIGHT_ARROW_BTN, HARD_MODE_BTN, QUIT_BTN] = create_button([
                (None, (75, 15), f"{self.get_player().get_username()}", 15, "black","grey"), #CREATE PROFILE BUTTON
                (None, (75, 100), "SHOP", 20, "black","grey"), #CREATE SHOP BUTTON
                (LEFT_ARROW_IMAGE_PATH, (400, 550), "", 75, "black","grey"), #CREATE LEFT BUTTON
                (RIGHT_ARROW_IMAGE_PATH, (860, 550), "", 75, "black","grey"), #CREATE RIGHT BUTTON
                (None, (640, 550), "HARD", 50, "black","grey"), #CREATE RIGHT BUTTON
                (None, (1175, 30), "EXIT GAME", 20, "black","grey") #CREATE QUIT BUTTON
                ])
            
            button_list_with_class_for_menu = [
                (USERNAME_BTN,ProfileMenu(self.get_player()), False),
                (SHOP_BTN,ShopMenu(self.get_player()), False),
                (HARD_MODE_BTN, Hard(self.get_player()), True), 
                (LEFT_ARROW_BTN,HardMenu(self.get_player()), False),
                (RIGHT_ARROW_BTN,MediumMenu(self.get_player()), False),
                ]
            update_screen_with_button_and_change_button_color(button_list,MENU_MOUSE_POS)
            check_mouse_input(button_list_with_class_for_menu, QUIT_BTN, MENU_MOUSE_POS, self) 
            pygame.display.update()
```

# No 7

Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table (Lampirkan diagram terkait)

Jawab:

Proses bisnis dalam project yang saya buat, pengguna aplikasi akan diarahkan untuk memilih mode permainan Easy, Medium ataupun Hard yang mana class-class tersebut menginherit class Menu yang di dalamnya terdapat atribut private dengan nama is_loopable dan atribut private dengan nama player. Class Menu tersebut menginherit class MenuAbstract yang mana class tersebut menginherit suatu class untuk membuat class MenuAbstract menjadi suatu kelas yang bersifat abstrak. Di dalam class MenuAbstract tersebut terdapat fungsi menu_started yang bersifat abstrak pula yang mana hal itu membuat fungsi tersebut harus diimplementasikan oleh class EasyMenu, HardMenu, MainMenu dan class MediumMenu. Setelah pengguna memilih mode permainan, pengguna akan menunggu permainan hingga selesai sampai sistem menunjukkan angka 90. Apabila angka menunjukkan 90, permainan akan berakhir. Setiap nilai dari 0 sampai ke 90, sistem akan melakukan random untuk menentukan apakah akan terjadi peristiwa gol ataupun tidak. Apabila suatu kondisi terjadinya peristiwa gol memenuhi kondisi-kondisi tertentu, maka akan terjadi peristiwa gol. Setelah itu, sistem kembali akan melakukan random untuk menentukan apakah gol akan berpihak ke player ataupun ke pihak musuh dengan kondisi-kondisi tertentu. 

Atribut-atribut yang bersifat private yang ada di dalam class EasyMenu, MainMenu, MediumMenu dan HardMenu tersebut tidak dapat diakses oleh objek lain. Untuk mengakses atribut-atribut tersebut, dapat diakses oleh fungsi yang mengembalikan nilai pada atribut-atribut yang bersifat private tadi

# No 8

Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Jawab:

ada didalam folder screenshoot

# No 9

Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video (Lampirkan link Youtube terkait)

Jawab:

# No 10

Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

Jawab:

ada dalam folder screenshoot

