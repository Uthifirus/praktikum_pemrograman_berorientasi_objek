from abc import ABC, abstractmethod
class PlayerAbstract(ABC):
    @abstractmethod
    def buy_diamond(self):
        pass
    
    @abstractmethod
    def buy_hero(self):
        pass
    
    @abstractmethod
    def change_username(self):
        pass
    
    # @abstractmethod
    # def change_password(self):
    #     pass
        

class Player(PlayerAbstract):
    def __init__(self, 
                 username: str,
                 atk_power: int,
                 def_power: int,
                 coins: int
                 ):
        self.__username = username
        self.__atk_power = atk_power
        self.__def_power = def_power
        self.__coins = coins
        super().__init__()
    
    def get_username(self):
        return self.__username
    
    def get_atk_power(self):
        return self.__atk_power
    
    def get_def_power(self):
        return self.__def_power
    
    
    def get_coins(self):
        return self.__coins
    
    
    def buy_diamond(self):
        pass
    
    def buy_hero(self):
        pass
    
    def set_current_rank(self, value: int):
        self.__current_rank += value
        
    def set_atk_power(self, value: int):
        self.__atk_power += value
    
    def set_def_power(self, value: int):
        self.__def_power += value
        
    def set_coins(self,value: int):
        self.__coins += value
    
    def change_username(self):
        pass
    
    # def change_password(self):
    #     pass