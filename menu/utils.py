import pygame

import json
import sys
import time

from button import Button
from utils import *
from user.user import Player

def create_user():
    base_font = pygame.font.Font(None, 32)
    warning_font = pygame.font.Font(None, 25)
    user_text = ''
    
    # create rectangle
    input_rect = pygame.Rect(800, 720//2, 2000, 32)
    color_active = pygame.Color('lightskyblue3')
  
    # color_passive store color(chartreuse4) which is
    # color of input box.
    color_passive = pygame.Color('chartreuse4')
    color = color_passive
    active = False
    users_data = {}
    is_user_added = False
    is_login_btn_clicked = False
    while True:
        SCREEN.blit(BG,(0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
    
            if event.type == pygame.MOUSEBUTTONDOWN:
                if input_rect.collidepoint(event.pos):
                    active = True
                else:
                    active = False
    
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    user_text = user_text[:-1]
                else:
                    if len(user_text) >= 15:
                        user_text = user_text
                    else:
                        user_text += event.unicode
                        with open(users_json_path) as all_users:
                            users_data = json.load(all_users)
        if active:
            color = color_active
        else:
            color = color_passive
        username_surf = base_font.render("Enter new username", True, (0,0,0))
        SCREEN.blit(username_surf, (800, input_rect.y-30))
        pygame.draw.rect(SCREEN, color, input_rect)
        if len(user_text) >= 15:
            user_exist_warning_surf = warning_font.render("Maximum username length is 15",True, (255, 0, 0))
            SCREEN.blit(user_exist_warning_surf, (800, input_rect.y+40))
        if len(user_text) == 0:
            user_exist_warning_surf = warning_font.render("Username cannot be empty",True, (255, 0, 0))
            SCREEN.blit(user_exist_warning_surf, (800, input_rect.y+40))
        if users_data.get(user_text) is not None:
            warning_surf = warning_font.render(f"Username {user_text} already exist", True, (255,0,0))
            SCREEN.blit(warning_surf, (1100, input_rect.y+10))
        text_surface = base_font.render(user_text, True, (255, 255, 255))
        
        SCREEN.blit(text_surface, (800+5, input_rect.y+5))
        input_rect.w = max(100, text_surface.get_width()+10)
        
        submit_btn = Button(None, (800+50,input_rect.y+100),"Sign Up", get_font(15),"black","white")
        login_btn = Button(None, (1000,input_rect.y+100),"Or Login", get_font(15),"black","white")
        submit_btn.update(SCREEN)
        login_btn.update(SCREEN)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                MENU_MOUSE_POS = pygame.mouse.get_pos()
                if submit_btn.checkForInput(MENU_MOUSE_POS):
                    if users_data.get(user_text) is None and not user_text == "":
                        new_user = {
                            user_text: {
                                "username": user_text,
                                "atk_power": 30,
                                "def_power": 30,
                                "coins": 3500
                            }
                        }
                        all_users_dict = {}
                        last_login_dict = {}
                        with open(r"json\users3.json") as all_users:
                            all_users_dict = json.load(all_users)
                        all_users.close()
                        all_users_dict.update(new_user)
                        with open(r"json\users3.json", "w") as outfile:
                            json.dump(all_users_dict, outfile, indent=4)
                        outfile.close()
                        print(f"data saved succesfully")
                        with open(r"json\last_login_user.json") as last_login:
                            last_login_dict = json.load(last_login)
                        last_login.close()
                        last_login_dict['last_login'] = user_text
                        with open(r"json\last_login_user.json", "w") as outfile:
                            json.dump(last_login_dict, outfile, indent=4)
                        outfile.close()
                        is_user_added = True
                if login_btn.checkForInput(MENU_MOUSE_POS):
                    is_login_btn_clicked = True
        if is_user_added or is_login_btn_clicked:
            break
        pygame.display.update()
    return is_login_btn_clicked
def login():
    base_font = pygame.font.Font(None, 32)
    warning_font = pygame.font.Font(None, 25)
    user_text = ''
    
    # create rectangle
    input_rect = pygame.Rect(800, 720//2, 2000, 32)
    color_active = pygame.Color('lightskyblue3')
  
    # color_passive store color(chartreuse4) which is
    # color of input box.
    color_passive = pygame.Color('chartreuse4')
    color = color_passive
    active = False
    users_data = {}
    user_exist = True
    login_btn_is_clicked = False
    while True:
        SCREEN.blit(BG,(0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
    
            if event.type == pygame.MOUSEBUTTONDOWN:
                if input_rect.collidepoint(event.pos):
                    active = True
                else:
                    active = False
    
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_BACKSPACE:
                    user_text = user_text[:-1]
                else:
                    if len(user_text) >= 15:
                        user_exist = True
                        user_text = user_text
                    else:
                        user_exist = True
                        user_text += event.unicode
                        with open(users_json_path) as all_users:
                            users_data = json.load(all_users)
                        
        if active:
            color = color_active
        else:
            color = color_passive
        username_surf = base_font.render("Enter username", True, (0,0,0))
        SCREEN.blit(username_surf, (800, input_rect.y-30))
        pygame.draw.rect(SCREEN, color, input_rect)
        if len(user_text) >= 15:
            user_exist_warning_surf = warning_font.render("Maximum username length is 15",True, (255, 0, 0))
            SCREEN.blit(user_exist_warning_surf, (800, input_rect.y+40))
        if len(user_text) == 0:
            user_exist_warning_surf = warning_font.render("Username cannot be empty",True, (255, 0, 0))
            SCREEN.blit(user_exist_warning_surf, (800, input_rect.y+40))
        # if users_data.get(user_text) is not None:
        #     warning_surf = warning_font.render(f"Username {user_text} already exist", True, (255,0,0))
        #     SCREEN.blit(warning_surf, (1100, input_rect.y+10))
        text_surface = base_font.render(user_text, True, (255, 255, 255))
        
        SCREEN.blit(text_surface, (800+5, input_rect.y+5))
        input_rect.w = max(100, text_surface.get_width()+10)
        login_btn = Button(None, (840,input_rect.y+100),"Login", get_font(15),"black","white")
        # login_btn = Button(None, (1000,input_rect.y+100),"Login", get_font(15),"black","white")
        login_btn.update(SCREEN)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                MENU_MOUSE_POS = pygame.mouse.get_pos()
                if login_btn.checkForInput(MENU_MOUSE_POS):
                    print(f"LOGIN BUTTON CLICKED FROM LOGIN FUNCTION")
                    with open(r'json\users3.json') as all_users:
                        all_users = json.load(all_users)
                    if all_users.get(user_text) is not None:
                        login_btn_is_clicked = True
                    if users_data.get(user_text) is None:
                        print(f"GET USER: {all_users.get(user_text)}")
                        user_exist = False
        if not user_exist:
            warning_surf = warning_font.render(f"Username {user_text} doesn't exist", True, (255,0,0))
            SCREEN.blit(warning_surf, (800, input_rect.y+30))
                        # print(f"GET USER BY INPUT: {all_users[user_text]}")
                        # print(f"ALL USERS: {all_users}")
        if login_btn_is_clicked:
            last_login = {
                "last_login": user_text
            }
            with open(r'json\last_login_user.json', 'w') as outfile:
                json.dump(last_login, outfile, indent=4)
            break
        pygame.display.update()
def get_font(size):
    return pygame.font.Font(FONT_PATH,size)

def create_left_right_arrow(left_arrow_pos: tuple, right_arrow_pos: tuple):
    LEFT_ARROW_BUTTON = Button(image=pygame.image.load(LEFT_ARROW_IMAGE_PATH),
                         pos=left_arrow_pos,
                         text_input="",
                         font=get_font(15),
                         base_color="#d7fcd4",
                         hovering_color="White"
                         )
    
    RIGHT_ARROW_BUTTON = Button(image=pygame.image.load(RIGHT_ARROW_IMAGE_PATH),
                         pos=right_arrow_pos,
                         text_input="",
                         font=get_font(15),
                         base_color="#d7fcd4",
                         hovering_color="White"
                         )
    
    return LEFT_ARROW_BUTTON, RIGHT_ARROW_BUTTON

def update_screen_with_button_and_change_button_color(button_list: list, MENU_MOUSE_POS: tuple):
    for button in button_list:
        button.changeColor(MENU_MOUSE_POS)
        button.update(SCREEN)

def check_mouse_input(button_list_with_class: list, QUIT_BUTTON: Button, MENU_MOUSE_POS: tuple, menu):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            for button in button_list_with_class:
                if button[2]:
                    if button[0].checkForInput(MENU_MOUSE_POS):
                        print(f"Print get is menu loopable: {menu.get_is_loopable()}")
                        menu.set_is_loopable(False)
                        print(f"Print get is menu loopable: {menu.get_is_loopable()}")
                        button[1].start_game()
                else:
                    if button[0].checkForInput(MENU_MOUSE_POS):
                        button[1].menu_started()
            if QUIT_BUTTON.checkForInput(MENU_MOUSE_POS):
                pygame.quit()
                sys.exit()
    pass

def create_button(btn_info_list: list):
    
    # CREATE USER PROFILE BUTTON
    button_list = list()
    for btn_info in btn_info_list:
        if btn_info[0] is None:
            image=None
        else:
            image=pygame.image.load(btn_info[0])
        new_button = Button(image=image,
                         pos=btn_info[1],
                         text_input=btn_info[2],
                         font=get_font(btn_info[3]),
                         base_color=btn_info[4],
                         hovering_color=btn_info[5]
                         )
        button_list.append(new_button)
    return button_list

def draw_bar(player: Player):
    SCREEN.blit(BG, (0,0))
    ATT_SURF = get_font(30).render("ATT POWER: ", True, "White")
    ATT_RECT = ATT_SURF.get_rect(topleft=(10,150))
    DEF_SURF = get_font(30).render("DEF POWER: ", True, "White")
    DEF_RECT = DEF_SURF.get_rect(topleft=(10,300))
    SCREEN.blit(ATT_SURF,ATT_RECT)
    SCREEN.blit(DEF_SURF,DEF_RECT)
    atk_bg_rect = pygame.Rect(300,120,700,80)
    pygame.draw.rect(SCREEN, "grey", atk_bg_rect)        
    atk_bg_bar_width = atk_bg_rect.width
    current_att_power = player.get_atk_power()
    current_atk_width = (current_att_power/100) * atk_bg_bar_width
    atk_rect = pygame.Rect(300, 120, current_atk_width, 80)
    pygame.draw.rect(SCREEN, "red",atk_rect)
    pygame.draw.rect(SCREEN, "black", atk_bg_rect, 3)
    
    
    def_bg_rect = pygame.Rect(300,270,700,80)
    pygame.draw.rect(SCREEN, "grey", def_bg_rect)    
    current_def_power = player.get_def_power()
    current_def_width = (current_def_power/100) * atk_bg_bar_width
    def_rect = pygame.Rect(300, 270, current_def_width, 80)
    pygame.draw.rect(SCREEN, "red",def_rect)
    pygame.draw.rect(SCREEN, "black", def_bg_rect, 3)
    ATT_POWER_TEXT = get_font(30).render(str(current_att_power), True, "white")
    ATT_POWER_SURF = ATT_POWER_TEXT.get_rect(topleft=(300+current_atk_width,200))
    SCREEN.blit(ATT_POWER_TEXT,ATT_POWER_SURF)
    DEF_POWER_TEXT = get_font(30).render(str(current_def_power), True, "white")
    DEF_POWER_SURF = DEF_POWER_TEXT.get_rect(topleft=(300+current_def_width,350))
    SCREEN.blit(DEF_POWER_TEXT,DEF_POWER_SURF)