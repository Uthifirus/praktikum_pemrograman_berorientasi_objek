from abc import ABC, abstractmethod
from button import Button
from utils import (write_per_character, clear_screen, get_font,
                   SCREEN, BG, PLAY_IMAGE_PATH, OPTION_IMAGE_PATH, QUIT_IMAGE_PATH, SHOP_IMAGE_PATH,
                   LEFT_ARROW_IMAGE_PATH, RIGHT_ARROW_IMAGE_PATH, FONT_PATH)
from menu.utils import *
from user.user import Player
from game.game import *
from game.utils import *

import sys
import pygame
import time
pygame.init()

class MenuAbstract(ABC):
    @abstractmethod
    def menu_started(self):
        pass
    

class Menu(MenuAbstract):
    def __init__(self, player: Player|None):
        self.__is_loopable = True
        self.__player = player
        
    def get_player(self):
        return self.__player
    
    def set_player(self, player: Player):
        self.__player = player
    
    def get_is_loopable(self):
        return self.__is_loopable
    
    def set_is_loopable(self, value:bool()):
        self.__is_loopable = value
        
class MainMenu(Menu):
    def __init__(self, player: Player):
        self.is_from_beginning=True
        super().__init__(player)
        
    
    def menu_started(self):
        if self.get_player() is None:
            is_login_btn_clicked = create_user()
            print(f"IS LOGIN BTN CLICKED: {is_login_btn_clicked}")
            if is_login_btn_clicked:
                login()
            last_user = get_last_login()
            print(f"GET LAST USER: {last_user}")
            user_account = get_user_account(last_user)
            print(f"GET USER ACCOUNT: {user_account}")
            player = Player(
                username=user_account['username'], 
                atk_power=user_account['atk_power'], 
                def_power=user_account['def_power'],
                coins=user_account['coins'])
            self.set_player(player)
        print(f"HELLO WORLD BEFORE WHILE SELF.IS_LOOPABLE")
        # time.sleep(3)
        while True:
            SCREEN.blit(BG,(0,0))
            
            MENU_MOUSE_POS = pygame.mouse.get_pos()
            
            # button_list = [USERNAME_BTN, SHOP_BTN,  QUIT_BTN] = create_button([
            button_list = [USERNAME_BTN, SHOP_BTN, LEFT_ARROW_BTN, RIGHT_ARROW_BTN, HARD_MODE_BTN, QUIT_BTN] = create_button([
                (None, (75, 15), f"{self.get_player().get_username()}", 15, "black","grey"), #CREATE PROFILE BUTTON
                (None, (75, 100), "SHOP", 20, "black","grey"), #CREATE SHOP BUTTON
                (LEFT_ARROW_IMAGE_PATH, (400, 550), "", 75, "black","grey"), #CREATE LEFT BUTTON
                (RIGHT_ARROW_IMAGE_PATH, (860, 550), "", 75, "black","grey"), #CREATE RIGHT BUTTON
                (None, (640, 550), "HARD", 50, "black","grey"), #CREATE RIGHT BUTTON
                (None, (1175, 30), "EXIT GAME", 20, "black","grey") #CREATE QUIT BUTTON
                ])
            
            button_list_with_class_for_menu = [
                (USERNAME_BTN,ProfileMenu(self.get_player()), False),
                (SHOP_BTN,ShopMenu(self.get_player()), False),
                (HARD_MODE_BTN, Hard(self.get_player()), True), 
                (LEFT_ARROW_BTN,HardMenu(self.get_player()), False),
                (RIGHT_ARROW_BTN,MediumMenu(self.get_player()), False),
                ]
            update_screen_with_button_and_change_button_color(button_list,MENU_MOUSE_POS)
            check_mouse_input(button_list_with_class_for_menu, QUIT_BTN, MENU_MOUSE_POS, self) 
            pygame.display.update()

class HardMenu(Menu):
    def __init__(self, player: Player):
        super().__init__(player)
    
    def menu_started(self):
        print(f"HELLO WORLD BEFORE WHILE SELF.IS_LOOPABLE")
        # time.sleep(3)
        while self.get_is_loopable():
            # print(f"Hard Menu")
            keys = pygame.key.get_pressed()
            if keys[pygame.K_q]:
                print(f"Q PRESSED")
                self.set_is_loopable(False)
            SCREEN.blit(BG,(0,0))
            
            MENU_MOUSE_POS = pygame.mouse.get_pos()
            
            button_list = [USERNAME_BTN, SHOP_BTN, LEFT_ARROW_BTN, RIGHT_ARROW_BTN, MEDIUM_MODE_BTN, QUIT_BTN] = create_button([
                (None, (75, 15), f"{self.get_player().get_username()}", 15, "black","grey"), #CREATE PROFILE BUTTON
                (None, (75, 100), "SHOP", 20, "black","grey"), #CREATE SHOP BUTTON
                (LEFT_ARROW_IMAGE_PATH, (400, 550), "", 75, "black","grey"), #CREATE LEFT BUTTON
                (RIGHT_ARROW_IMAGE_PATH, (860, 550), "", 75, "black","grey"), #CREATE RIGHT BUTTON
                (None, (640, 550), "MEDIUM" , 50, "black","grey"), #CREATE QUIT BUTTON
                (None, (1175, 30), "EXIT GAME", 20, "black","grey") #CREATE QUIT BUTTON
                ])
            
            button_list_with_class_for_menu = [
                (USERNAME_BTN, ProfileMenu(self.get_player()), False), 
                (SHOP_BTN, ShopMenu(self.get_player()), False), 
                (MEDIUM_MODE_BTN, Medium(self.get_player()), True), 
                (LEFT_ARROW_BTN, MediumMenu(self.get_player()), False), 
                (RIGHT_ARROW_BTN, MainMenu(self.get_player()), False), 
                ]
            
            update_screen_with_button_and_change_button_color(button_list,MENU_MOUSE_POS)
            check_mouse_input(button_list_with_class_for_menu, QUIT_BTN, MENU_MOUSE_POS, self) 
            pygame.display.update()
        print(f"HELLO WORLD AFTER WHILE SELF.IS_LOOPABLE")
    

class EasyMenu(Menu):
    def __init__(self, player: Player):
        super().__init__(player)
    def menu_started(self):
        print(f"HELLO WORLD BEFORE WHILE SELF.IS_LOOPABLE")
        # time.sleep(3)
        while self.get_is_loopable():
            keys = pygame.key.get_pressed()
            # print(f"Easy Menu")
            if keys[pygame.K_q]:
                print(f"Q PRESSED")
                self.set_is_loopable(False)
            SCREEN.blit(BG,(0,0))
            
            MENU_MOUSE_POS = pygame.mouse.get_pos()
            
            # button_list = [USERNAME_BTN, SHOP_BTN,  QUIT_BTN] = create_button([
            button_list = [USERNAME_BTN, SHOP_BTN, LEFT_ARROW_BTN, RIGHT_ARROW_BTN, EASY_MODE_BTN, QUIT_BTN] = create_button([
                (None, (75, 15), f"{self.get_player().get_username()}", 15, "black","grey"), #CREATE PROFILE BUTTON
                (None, (75, 100), "SHOP", 20, "black","grey"), #CREATE SHOP BUTTON
                (LEFT_ARROW_IMAGE_PATH, (400, 550), "", 75, "black","grey"), #CREATE LEFT BUTTON
                (RIGHT_ARROW_IMAGE_PATH, (860, 550), "", 75, "black","grey"), #CREATE RIGHT BUTTON
                (None, (640, 550), "MEDIUM", 50, "black","grey"), #CREATE QUIT BUTTON
                (None, (1175, 30), "EXIT GAME", 20, "black","grey") #CREATE QUIT BUTTON
                ])
            
            
            button_list_with_class_for_menu = [
                (USERNAME_BTN, ProfileMenu(self.get_player()), False), 
                (SHOP_BTN, ShopMenu(self.get_player()), False), 
                (LEFT_ARROW_BTN, HardMenu(self.get_player()), False), 
                (RIGHT_ARROW_BTN, MainMenu(self.get_player()), False), 
                ]
            update_screen_with_button_and_change_button_color(button_list,MENU_MOUSE_POS)
            check_mouse_input(button_list_with_class_for_menu, QUIT_BTN, MENU_MOUSE_POS, self) 
            pygame.display.update()

class MediumMenu(Menu):
    def __init__(self, player: Player):
        super().__init__(player)
        
    def menu_started(self):
        print(f"HELLO WORLD BEFORE WHILE SELF.IS_LOOPABLE")
        # time.sleep(3)
        while self.get_is_loopable():
            # print(f"Medium Menu")
            keys = pygame.key.get_pressed()
            if keys[pygame.K_q]:
                print(f"Q PRESSED")
                self.set_is_loopable(False)
            SCREEN.blit(BG,(0,0))
            
            MENU_MOUSE_POS = pygame.mouse.get_pos()
            button_list = [USERNAME_BTN, SHOP_BTN, LEFT_ARROW_BTN, RIGHT_ARROW_BTN, EASY_MODE_BTN, QUIT_BTN] = create_button([
                (None, (75, 15), f"{self.get_player().get_username()}", 15, "black","grey"), #CREATE PROFILE BUTTON
                (None, (75, 100), "SHOP", 20, "black","grey"), #CREATE SHOP BUTTON
                (LEFT_ARROW_IMAGE_PATH, (400, 550), "", 75, "black","grey"), #CREATE LEFT BUTTON
                (RIGHT_ARROW_IMAGE_PATH, (860, 550), "", 75, "black","grey"), #CREATE RIGHT BUTTON
                (None, (640, 550), "EASY", 50, "black","grey"), #CREATE QUIT BUTTON
                (None, (1175, 30), "EXIT GAME", 20, "black","grey") #CREATE QUIT BUTTON
                ])
            
            
            button_list_with_class_for_menu = [
                (USERNAME_BTN, ProfileMenu(self.get_player()), False), 
                (SHOP_BTN, ShopMenu(self.get_player()), False),
                (EASY_MODE_BTN, Easy(self.get_player()), True), 
                (LEFT_ARROW_BTN, MainMenu(self.get_player()), False), 
                (RIGHT_ARROW_BTN, HardMenu(self.get_player()), False), 
                ]
            update_screen_with_button_and_change_button_color(button_list,MENU_MOUSE_POS)
            check_mouse_input(button_list_with_class_for_menu, QUIT_BTN, MENU_MOUSE_POS, self) 
            pygame.display.update()

class ProfileMenu(Menu):
    def __init__(self, player: Player):
        super().__init__(player)
    
    def menu_started(self):
        while self.get_is_loopable():
            SHOP_MOUSE_POS = pygame.mouse.get_pos()
            draw_bar(self.get_player())
            
            SHOP_BACK = Button(image=None, pos=(75, 50), 
                                text_input="Back", font=get_font(30), base_color="Black", hovering_color="grey")

            SHOP_BACK.changeColor(SHOP_MOUSE_POS)
            SHOP_BACK.update(SCREEN)
            
            LOGOUT_BUTTON = Button(image=None, pos=(1150, 50), 
                                text_input="Logout", font=get_font(30), base_color="Black", hovering_color="grey")

            LOGOUT_BUTTON.changeColor(SHOP_MOUSE_POS)
            LOGOUT_BUTTON.update(SCREEN)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if SHOP_BACK.checkForInput(SHOP_MOUSE_POS):
                        main_menu = MainMenu(self.get_player())
                        main_menu.menu_started()
                    if LOGOUT_BUTTON.checkForInput(SHOP_MOUSE_POS):
                        with open(r'json\last_login_user.json') as last_user:
                            last_user_dict = json.load(last_user)
                        last_user_dict['last_login'] = ""
                        with open(r'json\last_login_user.json', 'w') as outfile:
                            json.dump(last_user_dict, outfile,indent=4)
                        print(f"LAST USER FROM LOGOUT BUTTON GOT CLICKED: {last_user_dict}")
                        main_menu = MainMenu(None)
                        main_menu.menu_started()
            pygame.display.update()

class ShopMenu(Menu):
    def __init__(self, player: Player):
        super().__init__(player)
        
    def menu_started(self):
        while True:
            player_username = self.get_player().get_username()
            # print(f"player: {player_username}")
            SHOP_MOUSE_POS = pygame.mouse.get_pos()

            SCREEN.blit(BG, (0,0))
            # SCREEN.fill(BG, (0,0))

            USERNAME_TEXT = get_font(20).render(f"{player_username}", True, "Black")
            USERNAME_RECT = USERNAME_TEXT.get_rect(center=(640, 100))
            SHOP_TEXT = get_font(45).render("This is the SHOP screen.", True, "Black")
            SHOP_RECT = SHOP_TEXT.get_rect(center=(640, 260))
            SCREEN.blit(USERNAME_TEXT, USERNAME_RECT)
            SCREEN.blit(SHOP_TEXT, SHOP_RECT)

            SHOP_BACK = Button(image=None, pos=(640, 460), 
                                text_input="BACK", font=get_font(75), base_color="Black", hovering_color="Green")

            SHOP_BACK.changeColor(SHOP_MOUSE_POS)
            SHOP_BACK.update(SCREEN)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if SHOP_BACK.checkForInput(SHOP_MOUSE_POS):
                        main_menu = MainMenu(self.get_player())
                        main_menu.menu_started()

            pygame.display.update() 